import axios from 'axios'
import { useEffect, useState } from 'react'

const EmployeePage = () => {
  const url = 'http://localhost:4000/emp/get'
  const [employees, setEmployees] = useState([])

  useEffect(() => {
    getEmployees()
  }, [])

  const getEmployees = () => {
    axios.get(url).then((response) => {
      const result = response.data
      if (result['status'] == 'success') {
        setEmployees(result['data'])
      }
    })
  }

  return (
    <div>
      <h1>Employee List</h1>
      <table className="table table-stripped">
        <thead>
          <tr>
            <th>EmpIdBBBBB</th>
            <th>EmpNameBBBB</th>
            <th>SalaryBBB</th>
            <th>AgeBBBB</th>
          </tr>
        </thead>

        <tbody>
          {employees.map((employee) => {
            return (
              <tr>
                <td>{employee['empid']}</td>
                <td>{employee['emp_name']}</td>
                <td>{employee['salary']}</td>
                <td>{employee['age']}</td>
              </tr>
            )
          })}
        </tbody>
      </table>

      


    </div>
  )
}

export default EmployeePage
