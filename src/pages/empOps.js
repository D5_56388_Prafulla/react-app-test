import axios from 'axios'
import { useEffect, useState } from 'react'
import { Alert } from 'react-alert'

const EmployeeOps = () => {
  const url1 = 'http://localhost:4000/emp/add'
  const url2 = 'http://localhost:4000/emp/update/{id}'
  const url3 = 'http://localhost:4000/emp/delete/{id}'
  const [employees, setEmployees] = useState([])
  const [emp_name, setEmp_name] = useState('')
  const [salary, setSalary] = useState('')
  const [age, setAge] = useState('')

 
  const addEmployee = () => {
    if (emp_name.length == 0) {
      toast.warning('Please enter emp_name')
    } else if (salary.length == 0) {
      toast.warning('Please enter salary')
    } else if (age.length == 0) {
      toast.warning('Please enter age')

    } else {
      const body = {
        emp_name,
        age,
        salary
      }

      const url = `http://localhost:4000/emp/add`

      axios.post(url, body).then((response) => {
        const result = response.data
        console.log(result)
        if (result['status'] == 'success') {
          alert('Successfully signed up new user')
        } else {
          alert(result['error'])
        }
      })
    }
  }

  return (
    <div>
    <h1 className="title">Signup</h1>
    
    <div className="row">
      <div className="col" />
      <div className="col">
        <div className="form">
          <div className="mb-3">
            <input
              placeholder="Emp Name"
               onChange={(e) => {
                 setEmp_name(e.target.value)
               }}
              type="text"
              className="form-control"
            />
          </div>
    
          <div className="mb-3">
            <input
              placeholder="Age"
               onChange={(e) => {
                 setAge(e.target.value)
               }}
              type="Number"
              className="form-control"
            />
          </div>
    
          <div className="mb-3">
            <input
              placeholder="Salary"
               onChange={(e) => {
                 setSalary(e.target.value)
               }}
              type="Number"
              className="form-control"
            />
          </div>
   
    
          <div className="mb-3">
            <div>
              <small>
                <input
                  type="checkbox"
                  name="checkbox"
                  value="check"
                  id="agree"
                />
                I have read and agree to the{" "}
                {/* <Link to="/TermsAndConditions.pdf">Terms and Conditions</Link> */}
              </small>
            </div>
         
             <button onClick={signupUser} className="btn btn-primary"> 
              Signup
            </button>
            <br />
            <br />
            <div>
              {/* Already have an account? <Link to="/signin">Signin here.</Link> */}
            </div>
          </div>
        </div>
      </div>
      <div className="col" />
    </div>
    
    </div>
      );
    };
    

export default EmployeeOps
